'''
This class parses the query for syntax errors.
'''
import re
from Constants import Constants

class SqlParser:
    def __init__(self, sqlQuery):
        self.sqlQuery = sqlQuery
        
    def getGroups(self, r):
        return r.groups()
    
    '''
    This method parses the query and returns the group of values required for querying.
    Regular expressions are written, which is matched against the input string.
    if a match is found:
        we proceed.
    if no match is found:
        we throw an error.
    '''
        
    def parseSqlQuery(self):
        
        selectWithoutWhere = re.compile("^SELECT\s+([^\s^;]+\s*(?:,\s*[^\s^;]+\s*)*?\s*)\s+FROM\s+([^\s^;]+)\s*$", re.IGNORECASE)
        selectWithJoin = re.compile("^SELECT\s+\*\s+FROM\s+([^\s^;^,]+\s*,\s*[^\s^;^,]+)\s+WHERE\s+([^\s^;]+\s*=\s*[^\s^;]+)\s*$", re.IGNORECASE)
        createTable = re.compile("^CREATE\s+(" + Constants.FIELD_REGEX + ")\s*\(\s*(" + Constants.FIELD_REGEX + "\s+(?:INT|STRING)\s*(?:,\s*" + Constants.FIELD_REGEX + "\s+(?:INT|STRING)\s*)*?\s*)\s*\)\s*$", re.IGNORECASE)
        insertQuery = re.compile("^INSERT INTO\s+([^\s^;]+)\s*\(\s*([^;]+\s*(?:,\s*[^;]+\s*)*?\s*)\s*\)\s*$", re.IGNORECASE)
        selectWithWhere = re.compile("^SELECT\s+([^;]+)\s+FROM\s+([^\s^;^,]+)\s+WHERE\s+([^\s^;]+\s*=\s*[^\s^;]+)\s*$", re.IGNORECASE)
        viewTable = re.compile("^VIEW TABLES\s*$", re.IGNORECASE)
        describeTable = re.compile("^DESCRIBE\s+([^\s^;^,]+)\s*$", re.IGNORECASE)
        
        groups = ()
        queryType = ""
        if(createTable.match(self.sqlQuery)):
            # print "create table query"
            groups = SqlParser.getGroups(self, createTable.search(self.sqlQuery))
            queryType = Constants.CREATE_TABLE
        elif(insertQuery.match(self.sqlQuery)):
            # print "insert query"
            groups = SqlParser.getGroups(self, insertQuery.search(self.sqlQuery))
            queryType = Constants.INSERT_TABLE
        elif(selectWithoutWhere.match(self.sqlQuery)):
            # print "select query without where clause"
            groups = SqlParser.getGroups(self, selectWithoutWhere.search(self.sqlQuery))
            queryType = Constants.SELECT_WITHOUT_WHERE
        elif(selectWithWhere.match(self.sqlQuery)):
            # print "selecy query with where clause"
            groups = SqlParser.getGroups(self, selectWithWhere.search(self.sqlQuery))
            queryType = Constants.SELECT_WITH_WHERE
        elif(selectWithJoin.match(self.sqlQuery)):
            # print "selecy query with join clause"
            groups = SqlParser.getGroups(self, selectWithJoin.search(self.sqlQuery))
            queryType = Constants.SELECT_JOIN
        elif(viewTable.match(self.sqlQuery)):
            queryType = Constants.VIEW_TABLES
        elif(describeTable.match(self.sqlQuery)):
            groups = SqlParser.getGroups(self, describeTable.search(self.sqlQuery))
            queryType = Constants.DESCRIBE_TABLE
        else:
            print "There is some error in the Sql Syntax -- valid statements are:"
            print "CREATE table {filed type{, field type}}"
            print "INSERT INTO table {value{, value}}"
            print "SELECT * FROM table1, table2 WHERE field1 = field2"
            print "SELECT field {,field} FROM table"
            print "SELECT * FROM table WHERE filed = constant"
            print "exit/quit"
            print "VIEW TABLES"
            print "DESCRIBE table1"
        
        return groups, queryType
