from SqlProcessor import SqlProcessor
from Constants import Constants

class SQLSession():
    def __init__(self):
        print "This is SQL-like query simulation. You can CREATE a table, INSERT INTO it, or SELECT from it."
        print "Enter quit/exit to end the session."
        print "---------------------------------------------------"

        sqlProcessor = SqlProcessor()

        while(True):
            try:
                sql = raw_input("Query >> ")
                if(sqlProcessor.processSqlQuery(sql) == Constants.QUIT):
                    break
                print "---------------------------------------------------"
            except KeyboardInterrupt:
                break
            except Exception:
                print "Something went wrong, please check your input properly"
                break
        sqlProcessor.writeMetaData()