class Constants(object):
    SELECT_WITHOUT_WHERE = "selectWithoutWhere"
    SELECT_WITH_WHERE = "selectWithWhere"
    SELECT_JOIN = "selectJoin"
    INSERT_TABLE = "insert"
    CREATE_TABLE = "create"
    VIEW_TABLES = "viewtables"
    DESCRIBE_TABLE = "describeTable"
    QUIT = "quit"
    INT = "INT"
    STRING = "STRING"
    INVALID_DATA_TYPE = "INVALID"
    METADATA_PATH = "data/metadata.pickle"
    DATA_PATH = "data/"
    TABLE = "table"
    COLUMN = "column"
    FILE_NAME = "fileName"
    DATA_TYPE = "dataType"
    COL_NUMBER = "columnNumber"
    FIELD_REGEX = "(?!select|from|where|view|tables|describe|int|string|create$).[^\s^;^,]+"
    def __init__(self, params):
        '''
        Constructor
        '''
        