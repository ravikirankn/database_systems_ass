import cPickle as pickle
import os
from Constants import Constants
from SqlParser import SqlParser
from SqlOperations import SqlOperations
from collections import OrderedDict

class SqlProcessor():
    """ This class takes the query and processes it. """
    def __init__(self):
        if os.path.isfile(Constants.METADATA_PATH):
            with open(Constants.METADATA_PATH, 'rb') as handle:
                self.metadata = pickle.load(handle)
        else:
            self.metadata = OrderedDict()
            
    def processSqlQuery(self, sqlQuery):
        if(sqlQuery == "quit" or sqlQuery == "exit" or sqlQuery == "QUIT" or sqlQuery == "EXIT"):
            return Constants.QUIT
        else:
            # First we call SqlParser.py to get groups and type of query
            groups, queryType = SqlParser(sqlQuery).parseSqlQuery()
            if queryType:
                # Next we call SqlOperations.py to make sure the query can be done,
                # and then it will actually perform the operation
                # metadata is updated in the SqlOperations and written at the end of session
                SqlOperations(self.metadata, groups, queryType)
    
    def writeMetaData(self):
        #print self.metadata
        with open(Constants.METADATA_PATH, 'wb') as handle:
            pickle.dump(self.metadata, handle)