from Constants import Constants
from requests.structures import CaseInsensitiveDict
import re
import cPickle as pickle
import os
from tabulate import tabulate
from collections import OrderedDict

class SqlOperations():
    """ This class handles the query operations and makes sure they're executed. """
    def __init__(self, metadata, groups, query_type):
        """ Calls the right function for the query type - CREATE, INSERT INTO, or SELECT. """
        self.metadata = metadata
        self.stringRegex = re.compile("^['\"](.+?)['\"]$")
        self.digitRegex = re.compile("^-?[0-9]+$")
        if query_type == Constants.CREATE_TABLE:
            createTable(self, groups)
        elif query_type == Constants.SELECT_WITHOUT_WHERE:
            selectFromTable(self, groups)
        elif query_type == Constants.SELECT_WITH_WHERE:
            selectFromTable(self, groups)
        elif query_type == Constants.INSERT_TABLE:
            insertIntoTable(self, groups)
        elif query_type == Constants.SELECT_JOIN:
            selectWithJoin(self, groups)
        elif query_type == Constants.VIEW_TABLES:
            viewTables(self)
        elif query_type == Constants.DESCRIBE_TABLE:
            describeTable(self, groups)
        else:
            print "Invalid query type."

def viewTables(self):
    """ Show all the tables present."""
    list1 = [['Table Name']]
    for key , value in self.metadata.iteritems():
        if [value[Constants.TABLE]] not in list1:
            list1.append([value[Constants.TABLE]])
    print tabulate(list1, headers="firstrow", tablefmt="grid")

def describeTable(self, groups):
    """ Descrcibe the table """
    data = [['Column Name', 'Data Type']]
    for key , value in self.metadata.iteritems():
        if groups[0].strip().lower() == key.rsplit('_', 1)[0].lower():
            data.append([value[Constants.COLUMN], value[Constants.DATA_TYPE]])
    print tabulate(data, headers="firstrow", tablefmt="grid")

def createTable(self, groups):
    """ Creates a table; first checks metadata to see if table exists, ignoring case; updates metadata. """
    table_name = groups[0]
    if(checkMetadataForTableName(self, table_name)):
        print "Error: Table already exists."
    else:
        column_list = groups[1].split(",")
        count = 1
        for column in column_list:
            name_datatype = column.strip().split()
            self.metadata[table_name.strip() + '_' + name_datatype[0].strip()] = {Constants.TABLE:table_name.strip(),
                                                                                  Constants.COLUMN:name_datatype[0].strip(),
                                                                                  Constants.DATA_TYPE:name_datatype[1].strip(),
                                                                                  Constants.COL_NUMBER:count,
                                                                                  Constants.FILE_NAME:str(table_name.strip()) + ".pickle"}
            count = count + 1

def insertIntoTable(self, groups):
    """ Insert values into the table. """
    table_name = groups[0].strip()
    if not checkMetadataForTableName(self, table_name):
        print "Error: Table ", table_name , " doesn't exist"
        return
    count = 1
    insert_element = OrderedDict()  # creates Python dictionary
    filename = ""
    # print groups[1], re.split(r', (?=(?:"[^"]*?(?: [^"]*)*))|, (?=[^",]+(?:,|$))', groups[1])
    for insert_value in re.split(r', (?=(?:"[^"]*?(?: [^"]*)*))|,\s*(?=[^",]+(?:,|$))', groups[1]):
        data_type_check = getDataType(self, insert_value.strip())
        if data_type_check == Constants.INVALID_DATA_TYPE:
            print "Error: Invalid insert values, they doesn't match with columns datatype"
            return
        condition, column_name, filename = checkMetadataForColumnNames(self, table_name, count, data_type_check)
        if(condition):
            insert_element[column_name] = insert_value.strip().strip("'|\"")
        else:
            print "Error: Cannot insert specified values; they are not in alignment with the schema."
            return
        count = count + 1
    writeToPickle(insert_element, filename)

def selectFromTable(self, groups):
    """ Performs SELECT for queries. """
    table_name = groups[1].strip()
    if not checkMetadataForTableName(self, table_name):
        print "Error: Table ", table_name , " doesn't exist"
        return
    project_list = list(groups[:-1])[0].split(",")
    if not project_list[0] == "*":
        for col_name in project_list:
            if not checkMetadataForColumns(self, col_name.strip(), table_name):
                print "Error: Column", col_name, "not found in table", table_name
                return
    else:
        project_list = build_column_list(self, table_name)
    try:
        select_list = groups[2].split("=")
        if not checkMetadataForColumns(self, select_list[0].strip(), table_name):
            print "Error: Column", select_list[0].strip(), "not found in table", table_name
            return
        print_data(project_list, select_list , loadFromPickle(getFileNameFromMetadata(self, table_name)))
    except IndexError:
        print_data(project_list, None, loadFromPickle(getFileNameFromMetadata(self, table_name)))
    
def selectWithJoin(self, groups):
    """ Incomplete: Performs SELECT from multiple tables. """
    table_list = groups[0].split(",")
    select_list = groups[1].split("=")
    # print "Tables: ", table_list, "Selection: ", select_list
    for table_name in table_list:
        if not checkMetadataForTableName(self, table_name.strip()):
            print "Error: Table ", table_name , " doesn't exist"
            return
    for i in range(1, 2):
        if not checkMetadataForColumns(self, select_list[i].strip(), table_list[i].strip()):
                print "Error: Column", select_list[i].strip(), "not found in table", table_list[i].strip()
                return
    dict1 = loadFromPickle(getFileNameFromMetadata(self, table_list[0].strip()))
    dict2 = loadFromPickle(getFileNameFromMetadata(self, table_list[1].strip()))
    dictMain = OrderedDict()
    # print dict1, dict2
    for key1, value1 in dict1.iteritems():
        for key2, value2 in dict2.iteritems():
            if not CaseInsensitiveDict(value1)[select_list[0].strip()] == CaseInsensitiveDict(value2)[select_list[1].strip()]:
                continue
            for key3, value3 in value1.iteritems():
                if key3 in dictMain:
                    dictMain[key3].append(value3)
                else:
                    dictMain[key3] = [value3]
            for key4, value4 in value2.iteritems():
                if key4 + "1" in dictMain:
                    dictMain[key4 + "1"].append(value4)
                else:
                    dictMain[key4 + "1"] = [value4]

    if not dictMain:
        print "No data to print"
    else:
        print tabulate(dictMain, headers="keys", tablefmt="grid")
# --------------------------------------------------------------------------------------
# Utility functions:

def checkMetadataForTableName(self, table_name):
    """ Checks to see if table name exists; returns True or False. """
    for key , value in self.metadata.iteritems():
        if(value[Constants.TABLE].lower() == table_name.strip().lower()):
            return True
    return False

def getDataType(self, value):
    """ Returns data type of value """
    if self.stringRegex.match(value):
        return Constants.STRING
    elif self.digitRegex.match(value):
        return Constants.INT
    else:
        return Constants.INVALID_DATA_TYPE

def getFileNameFromMetadata(self, table_name):
    for key, value in self.metadata.iteritems():
        if table_name.lower() == key.rsplit('_', 1)[0].lower():
            return value[Constants.FILE_NAME]

def checkMetadataForColumnNames(self, table_name, column_number, data_type):
    """ Returns T/F for if columns exist, returns column name, and returns file name. """
    selectedMetadata = {}
    for key, value in self.metadata.iteritems():
        if table_name.lower() == key.rsplit('_', 1)[0].lower():
            selectedMetadata[value[Constants.COL_NUMBER]] = value
    return data_type.lower() == (selectedMetadata[column_number][Constants.DATA_TYPE]).lower() , selectedMetadata[column_number][Constants.COLUMN], selectedMetadata[column_number][Constants.FILE_NAME]

def checkMetadataForColumns(self, column_name, table_name):
    """ Checks to see if column names exist; returns True or False. """
    return table_name + "_" + column_name in CaseInsensitiveDict(self.metadata)

def checkMetadataForColumnDataType(self, table_name, column_name, data_type):
    """ Checks if the column type matches what's expected. """
    return data_type.strip() == CaseInsensitiveDict(self.metadata)[table_name.strip() + "_" + column_name.strip()][Constants.DATA_TYPE]

def writeToPickle(value, fileName):
    """ Writes to pickle file. """
    dict1 = OrderedDict()
    if os.path.isfile(Constants.DATA_PATH + fileName):
        dict1 = pickle.load(open(Constants.DATA_PATH + fileName, "rb"))
    key = 1
    if(dict1):
        key = max(dict1.keys(), key=int) + 1
    dict1[key] = value
    pickle.dump(dict1, open(Constants.DATA_PATH + fileName, "wb"))

def loadFromPickle(fileName):
    """ Reads from pickle file given filename and returns data dictionary. """
    if os.path.isfile(Constants.DATA_PATH + fileName):
        dict1 = pickle.load(open(Constants.DATA_PATH + fileName, "rb"))
        return dict1
    else:
        # print "No data returned for", fileName
        return OrderedDict()

def build_column_list(self, table_name):
    """ Returns all the columns that exist for a table. """
    column_list = []
    for key, value in self.metadata.iteritems():
        if table_name.lower() == key.rsplit('_', 1)[0].lower():
            column_list.append(value[Constants.COLUMN])
    column_list.reverse()
    # print "Column List:", column_list
    return column_list

def print_data(projection, selection, data_list):
    """ Prints SELECT data. """
    dict1 = OrderedDict()
    if not data_list:
        print tabulate([projection], headers="firstrow", tablefmt="grid")
        return
    for key, value in data_list.iteritems():
        caseSensitiveValue = CaseInsensitiveDict(value)
        if selection is not None:
            if not caseSensitiveValue[selection[0].strip()] == selection[1].strip():
                continue
        for key2, value2 in value.iteritems():
            if key2.lower() in map(str.strip , map(str.lower, projection)):
                if key2 in dict1:
                    dict1[key2].append(value2)
                else:
                    dict1[key2] = [value2]
    print tabulate(dict1, headers="keys", tablefmt="grid")
