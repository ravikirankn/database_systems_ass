Instructions:
(1) Install Python 2.7
(2) Make sure that the requests package is installed: sudo pip install requests
(3) Make sure that tabulate package is installed: sudo pip install tabulate
(4) Then type this to actually start the program: sh run.sh

Please note that if the program quits unexpectedly, exception is caught and below message is shown
--"Something went wrong, please check your input properly"

----------------------------------------------------------------------------------------------------
Class Description:

SqlProcessor.py --
Takes the SQL-like query and processes it; it calls methods in SqlParser.py for parsing the syntax 
and SqlOperations.py to perform the desired operation.

SqlParser.py --
Takes the query and verifies the syntax.

SqlOperations.py --
Takes the query (if it is valid) after the processing and performs the operations.

testRunner.py --
A test program to run the code.

Constants.py --
Provides a clean way to define things, such as METADATA_PATH = "data/metadata.pickle"

SQLSession.py --
Runs the session simulation.

----------------------------------------------------------------------------------------------------

Data Storage:

--Data is stored in a pickle file. Pickling is a form of serialization, and the Python object is converted to a byte stream.
  Unpickling is the opposite, deserialization, so unpickling reads the byte stream and converts it back to the data's original format.
  Please refer to the Python documentation for more information.

--Data dictionary mentioned in the assignment is stored in data/metadata.pickle.

-- metadata.pickle is loaded at the start of the session into a python dictionary, modified over the course of time and again written 
   back to the file at the end of session on quit/exit or keyboardInterrupt
   eg: supposed there is one table user and it has two columns id(INT) and name(STRING) then the metadat looks like.
   {'user_id': {'column': 'id', 'table': 'user', 'fileName': 'user.pickle', 'dataType': 'INT', 'columnNumber': 1}, 
   'user_name': {'column': 'name', 'table': 'user', 'fileName': 'user.pickle', 'dataType': 'STRING', 'columnNumber': 2}}

-- Data in the tables is loaded in individual pickle files, eg: books.pickle.

----------------------------------------------------------------------------------------------------
# Example queries:

1. CREATE books( callno INT, title STRING, authors STRING, year INT)
2. VIEW TABLES
3. DESCRIBE books
4. INSERT INTO books (1, 'The Devils Dictionary', 'Ambrose Bierce', 1998)
5. SELECT * FROM books
6. SELECT callno, title FROM books
7. SELECT * FROM books WHERE year = 1998
8. SELECT callno, title FROM books WHERE callno=100
9. SELECT * FROM user1,user2 WHERE id=id

----------------------------------------------------------------------------------------------------
Contribution:

Ravi Kiran (PSID: 1361153)—

1. Started off the project by writing regular expressions to parse the sql queries, query string is matched against the regular expression which groups the required filed names and values.
The groups are returned and passed to the SqlOperations for further processing
2. Implemented select query with selection and projection criteria
3. Implemented join. (select  * from table1,table2 where id1=id2)
4. (Optional) Implemented View tables, describe table1 to easily understand already existing table structure

Dallas Kidd (PSID: 0496049)—

1. Started by working on SqlOperations took the groups returned from SqlParser and started processing the data
2. Worked on the data dictionary (metadata), loading it before session and writing it after session
3. Implemented create table query
4. Implemented insert into table (Writing into pickle file)
