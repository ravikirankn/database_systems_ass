package main;

import java.io.FileNotFoundException;
import java.io.IOException;

public class TestRunner {

  public static void main(String[] args) throws FileNotFoundException, IOException {
    Processor processor = new Processor();
    if (args != null && args.length != 0 && args[0] != null && !"".equalsIgnoreCase(args[0])) {
      processor.processFromFile(args[0]);
    } else {
      processor.processFromConsole();
    }
  }
}
