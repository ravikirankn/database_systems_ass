package main;

public interface Constants {
  public final String INSERT = "*insert";
  public final String UPDATE = "*update";
  public final String DELETE = "*delete";
  public final String LIST = "*list";
  public final String SEARCH = "*search";
  public final String SNAPSHOT = "*snapshot";
}
