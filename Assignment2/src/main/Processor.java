package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Processor {

  public void processFromFile(String filename) throws FileNotFoundException, IOException {

    SyntaxParser parser = new SyntaxParser();
    OperationRouter router = new OperationRouter();

    BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

    String input = null;
    int count1 = 0;
    String updateKey = "";

    while ((input = br.readLine()) != null) {
      System.out.println("-------------------------------");
      System.out.println("Entered keyword/record: " + input);
      if (terminationCondition(input) && count1 == 0) {
        break;
      } else {
        input = parser.parse(input);
        if (count1 == 0) {
          router.route(parser.OPERATION, input.toLowerCase());
          if (Constants.UPDATE.equalsIgnoreCase(parser.OPERATION)) {
            count1++;
          }
        } else if (count1 == 1) {
          updateKey = input.toLowerCase();
          count1++;
        } else if (count1 == 2) {
          router.route(Constants.UPDATE, updateKey, input);
          count1 = 0;
        }
      }
    }
    br.close();
  }

  public void processFromConsole() throws IOException {

    SyntaxParser parser = new SyntaxParser();
    OperationRouter router = new OperationRouter();

    int count1 = 0;
    String updateKey = "";

    while (true) {
      System.out.println("-------------------------------");
      System.out.print("Enter keyword/record: ");
      String input = getInputFromConsole();
      if (terminationCondition(input) && count1 == 0) {
        break;
      } else {
        input = parser.parse(input);
        if (count1 == 0) {
          router.route(parser.OPERATION, input.toLowerCase());
          if (Constants.UPDATE.equalsIgnoreCase(parser.OPERATION)) {
            count1++;
          }
        } else if (count1 == 1) {
          updateKey = input.toLowerCase();
          count1++;
        } else if (count1 == 2) {
          router.route(Constants.UPDATE, updateKey, input);
          count1 = 0;
        }
      }
    }
  }

  private boolean terminationCondition(String input) {
    return input.equalsIgnoreCase("exit") || input.equalsIgnoreCase("quit");
  }

  private String getInputFromConsole() throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    return br.readLine();
  }

}
