package main;

import tree.BPlusTree;
import tree.Tree;
import tree.Tree.KeyValue;

public class OperationRouter implements Constants {

  private Tree tree = new BPlusTree();

  public void route(String operation, String input) {
    if (operation != null && input != null && !"".equalsIgnoreCase(input)) {
      switch (operation) {
        case INSERT:
          tree.put(input, valueToPut());
          break;
        case DELETE:
          tree.delete(input);
          break;
        case SEARCH:
          KeyValue child = tree.get(input);
          if (child != null) {
            System.out.println("Key: " + child.getKey());
            System.out.println("Value: " + child.getValue());
          } else {
            System.out.println("Key doesn't exist.");
          }
          break;
        default:
          System.out.println("Operation not found.");
          break;
      }
    } else if (operation != null && (input == null || "".equalsIgnoreCase(input))) {
      switch (operation) {
        case LIST:
          tree.list();
          break;
        case SNAPSHOT:
          tree.snapshot();
          break;
        case INSERT:
        case UPDATE:
        case DELETE:
        case SEARCH:
          // System.out.println("About to " + operation + " ..");
          break;
        default:
          System.out.println("Operation not found.");
          break;
      }
    } else if (operation == null) {
      System.out.println("Invalid operation.");
    }
  }

  private String valueToPut() {
    String str = "";
    for (int i = 0; i < 224; i++) {
      str = str + " ";
    }
    // System.out.println("Inserting " + str.length() + " blank chars.");
    return str;
  }

  public void route(String update, String inputkey, String input) {
    tree.update(inputkey, input);
  }
}
