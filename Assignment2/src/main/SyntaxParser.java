package main;

public class SyntaxParser implements Constants {
  public String OPERATION = null;

  public String parse(String input) {
    if (condition(input.toLowerCase())) {
      OPERATION = input.toLowerCase();
      return "";
    } else if (validateInput(input)) {
      return input;
    } else {
      return "";
    }
  }

  private boolean validateInput(String input) {
    return input.length() <= 32;
  }

  private boolean condition(String operation) {
    return (operation.equalsIgnoreCase(INSERT) || operation.equalsIgnoreCase(UPDATE)
        || operation.equalsIgnoreCase(DELETE) || operation.equalsIgnoreCase(LIST) || operation.equalsIgnoreCase(SEARCH) || operation
          .equalsIgnoreCase(SNAPSHOT));
  }
}
