package tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;


public class BPlusTree implements Tree {

  private static final int maxChildren = 5;
  private int height;
  private int size;

  public int height() {
    return height;
  }

  public int size() {
    return size;
  }

  public class Node {
    private int numKeys = 0;
    private int depth = 1;
    private String[] keys = new String[2 * maxChildren];
    private String[] values = new String[2 * maxChildren];
    private Node[] children = new Node[2 * maxChildren + 1];
    private boolean isLeaf;
    private Node nextNode;
    public int getNumKeys() {
      return numKeys;
    }
    public void setNumKeys(int numKeys) {
      this.numKeys = numKeys;
    }
    public int getDepth() {
      return depth;
    }
    public void setDepth(int depth) {
      this.depth = depth;
    }
    public String[] getKeys() {
      return keys;
    }
    public void setKeys(String[] keys) {
      this.keys = keys;
    }
    public String[] getValues() {
      return values;
    }
    public void setValues(String[] values) {
      this.values = values;
    }
    public Node[] getChildren() {
      return children;
    }
    public void setChildren(Node[] children) {
      this.children = children;
    }
    public boolean isLeaf() {
      return isLeaf;
    }
    public void setLeaf(boolean isLeaf) {
      this.isLeaf = isLeaf;
    }
    public Node getNextNode() {
      return nextNode;
    }
    public void setNextNode(Node nextNode) {
      this.nextNode = nextNode;
    }
    
    
  }

  private Node root;

  public BPlusTree() {
    root = new Node();
    root.isLeaf = true;
  }

  @Override
  public void put(String key, String value) {
    if (get(key) != null) {
      System.out.println("Key already exists.");
    } else {
      Node rootNode = root;
      size++;
      if (rootNode.numKeys == (2 * maxChildren)) {
        Node newRootNode = new Node();
        root = newRootNode;
        newRootNode.isLeaf = false;
        root.children[0] = rootNode;
        splitNode(newRootNode, 0, rootNode);
        insert(newRootNode, key, value);
        height++;
      } else {
        insert(rootNode, key, value);
      }
    }
  }

  private void insert(Node node, String key, String value) {
    int i = node.numKeys - 1;
    if (node.isLeaf) {
      while (i >= 0 && less(key, node.keys[i])) {
        node.keys[i + 1] = node.keys[i];
        node.values[i + 1] = node.values[i];
        i--;
      }
      i++;
      node.keys[i] = key;
      node.values[i] = value;
      node.numKeys++;
    } else {
      while (i >= 0 && less(key, node.keys[i])) {
        i--;
      }
      i++;
      //System.out.println(node.children[i]);
      if (node.children[i].numKeys == (2 * maxChildren)) {
        splitNode(node, i, node.children[i]);
        if (more(key, node.keys[i])) {
          i++;
        }
      }
      insert(node.children[i], key, value);
    }
  }

  private void splitNode(Node parent, int i, Node node) {
    Node newNode = new Node();
    newNode.isLeaf = node.isLeaf;
    newNode.numKeys = maxChildren + 1;
    for (int j = 0; j < maxChildren + 1; j++) {
      //System.out.println("splitNode: "+node.keys[j + maxChildren - 1]);
      newNode.keys[j] = node.keys[j + maxChildren - 1];
      newNode.values[j] = node.values[j + maxChildren - 1];
    }
    if (!newNode.isLeaf) {
      for (int j = 0; j < maxChildren + 2; j++) {
        //System.out.println("splitNode1: "+node.children[j + maxChildren - 1]);
        newNode.children[j] = node.children[j + maxChildren - 1];
      }
      for (int j = maxChildren; j <= node.numKeys; j++) {
        node.children[j] = null;
      }
    } else {
      newNode.nextNode = node.nextNode;
      node.nextNode = newNode;
    }
    for (int j = maxChildren - 1; j < node.numKeys; j++) {
      node.keys[j] = "";
      node.values[j] = null;
    }
    node.numKeys = maxChildren - 1;
    for (int j = parent.numKeys; j >= i + 1; j--) {
      parent.children[j + 1] = parent.children[j];
    }
    parent.children[i + 1] = newNode;
    for (int j = parent.numKeys - 1; j >= i; j--) {
      parent.keys[j + 1] = parent.keys[j];
      parent.values[j + 1] = parent.values[j];
    }
    parent.keys[i] = newNode.keys[0];
    parent.values[i] = newNode.values[0];
    parent.numKeys++;
  }

  /**
   * compare keys
   * 
   * @param k1
   * @param k2
   * @return
   */

  private boolean less(String k1, String k2) {
    return k1.compareTo(k2) < 0;
  }

  private boolean more(String k1, String k2) {
    return k1.compareTo(k2) > 0;
  }

  /**
   * get key
   * 
   * @param key
   * @return
   */

  public KeyValue get(String key) {
    return search(root, key);
  }

  public KeyValue search(Node node, String key) {
    int i = 0;
    while (i < node.numKeys && more(key, node.keys[i])) {
      i++;
    }
    if (i < node.numKeys && key.equalsIgnoreCase(node.keys[i])) {
      return new KeyValue(node.keys[i], node.values[i]);
    }
    if (node.isLeaf) {
      return null;
    } else {
      return search(node.children[i], key);
    }
  }

  /**
   * update key
   */

  @Override
  public void update(String key, String value) {
    if (update(root, key, value) == null) {
      System.out.println("Key doesn't exist.");
    }
  }

  private String update(Node node, String key, String value) {
    int i = 0;
    while (i < node.numKeys && more(key, node.keys[i])) {
      i++;
    }
    if (i < node.numKeys && key.equalsIgnoreCase(node.keys[i])) {
      node.values[i] = value;
      return node.values[i];
    }
    if (node.isLeaf) {
      return null;
    } else {
      return update(node.children[i], key, value);
    }
  }

  /**
   * Delete: First the key and value are marked to null. If a node contains no valid key, then the node is deleted.
   */

  @Override
  public void delete(String key) {
    Node deleteFrom = delete(getFirstLeafNode(root), key);
    if (deleteFrom == null) {
      System.out.println("Key doesn't exist.");
    } else {
      size--;
      // deleteEmptyNodesRecuresvly(deleteFrom);
      deleteEmptyNodess(root);
      balanceTree(root);
      checkRoot();
    }
  }

  private void balanceTree(Node node) {
    if(node.numKeys==0 && node.children[0]!=null){
      //System.out.println("am here...");
      setNode(node, node.children[0]);
    }
    for(int i = 0; i < node.numKeys + 1 && node.children[i] != null; i++){
      balanceTree(node.children[i]);
    }
  }
  
  private void setNode(Node node1, Node node2){
    node1.setChildren(node2.getChildren());
    node1.setKeys(node2.getKeys());
    node1.setLeaf(node2.isLeaf());
    node1.setNextNode(node2.getNextNode());
    node1.setNumKeys(node2.getNumKeys());
    node1.setValues(node2.getValues());
  }

  private void treeAsMap(Node node, Map<Integer, List<Node>> treeMap) {
      if(treeMap.containsKey(node.depth)){
        List<Node> list = treeMap.get(node.depth);
        list.add(node);
        treeMap.put(node.depth, list);
      }else{
        List<Node> list = new ArrayList<>();
        list.add(node);
        treeMap.put(node.depth, list);
      }
      for (int i = 0; i < node.numKeys + 1 && node.children[i] != null; i++) {
        node.children[i].depth = node.depth + 1;
        treeAsMap(node.children[i],treeMap);
      }
  }

  private Node delete(Node node, String key) {
    for (int i = 0; i < node.keys.length && node.isLeaf; i++) {
      if (key.equalsIgnoreCase(node.keys[i])) {
        deleteNode(node, i);
        //System.out.println("Deleted: " + node.isLeaf + " " + node.numKeys);
        return node;
      }
    }
    if (node.nextNode != null) {
      return delete(node.nextNode, key);
    }
    return null;
  }
  
  private void deleteNode(Node node, int i) {
    //System.out.println("am here..");
    node.values[i] = null;
    node.keys[i] = null;
    List<String> values = new ArrayList<String>(Arrays.asList(node.values));
    List<String> keys = new ArrayList<String>(Arrays.asList(node.keys));
    values.removeAll(Collections.singleton(null));
    keys.removeAll(Collections.singleton(null));
    node.keys = keys.toArray(node.keys);
    node.values = values.toArray(node.values);
    node.numKeys--;
  }
  
  private Node deleteEmptyNodess(Node node) {
    if (!node.isLeaf) {
      for (int i = 0; i < node.numKeys + 1 && node.children[i] != null; i++) {
        if (node.children[i] != null && node.children[i].numKeys == 0) {
          //System.out.println("deleting node, since it's empty " + i + node);
          //node.keys[i] = "";
          removeKeyBalanceNode(node, i);
          return node;
        }
      }
      for (int i = 0; i < node.numKeys + 1 && node.children[i] != null; i++) {
        deleteEmptyNodess(node.children[i]);
      }
    }
    return node;
  }
  
  private void checkRoot() {
    //System.out.println(node.children[0]);
    if (root.numKeys == -1) {
      root.isLeaf = true;
      root.numKeys = 0;
    }
  }
  
  private void removeKeyBalanceNode(Node node, int i) {
    List<String> keys = new ArrayList<String>(Arrays.asList(node.keys));
    if (getNumberOfChildren(node) == 1){
      //System.out.println("number of children: "+ getNumberOfChildren(node));
      //System.out.println("keys: " + keys);
      node = node.children[0];
    }else{
      //System.out.println("Number of keys: "+ node.numKeys);
      List<Node> children = new ArrayList<Node>(Arrays.asList(node.children));
      children.remove(i);
      node.children = children.toArray(node.children);
      //System.out.println("removing key: " + keys);
      keys.remove(i);
      node.keys = keys.toArray(node.keys);
      node.numKeys--;
    }
    
  }
  
  private int getNumberOfChildren(Node node){
    int count = 0;
    for(int i =0;i<node.children.length;i++){
      if(node.children[i]!=null){
        count++;
      }
    }
    return count;
  }
  
  @Override
  public void snapshot() {
    //Node leafNode = getFirstLeafNode(root);
    System.out.println("Number of records: " + size());
    System.out.println("Depth of the tree: " + getDepth());
    System.out.println("--------First last key of all nodes--------");
    root.depth = 1;
    printKeys();
    root.depth = 1;
    System.out.println("--------------End of keys------------------");
    System.out.println("Number of blocks occupied (#Records/4): " + Math.ceil(size()/4));
    System.out.println("Number of blocks (As in example given by TA): " + getNumberOfBlocks(getFirstLeafNode(root)));
  }
  
  
  /**
   * not used..
   * @param node
   * @param keyNumber
   */

  private void printFirstLastKeys(Node node, int keyNumber) {
    // System.out.println(node.numKeys);
    if (node.numKeys == 0) {
      System.out.println("Depth: " + node.depth + " Node: " + keyNumber + " First key: " + null + " Last key: " + null);
    } else if (node.numKeys == 1) {
      System.out.println("Depth: " + node.depth + " Node: " + keyNumber + " First key: " + node.keys[0] + " Last key: "
          + null);
    } else {
      System.out.println("Depth: " + node.depth + " Node: " + keyNumber + " First key: " + node.keys[0] + " Last key: "
          + node.keys[node.numKeys - 1]);
    }

    if (!node.isLeaf) {
      for (int i = 0; i < node.numKeys + 1 && node.children[i] != null; i++) {
        node.children[i].depth = node.depth + 1;
        printFirstLastKeys(node.children[i], i + 1);
      }
    }
  }
  
  /**
   * print first and last keys of all nodes.
   * @param node
   * @param keyNumber
   */
  
  private void printKeys(){
    Map<Integer, List<Node>>treeMap = new HashMap<>();
    treeAsMap(root,treeMap);
    Comparator<Integer> comp = new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        if(o1 > o2) return 1;
        if(o1 < o2) return -1;
        else return 0;
      }
      
    };
    SortedSet<Integer> keys = new TreeSet<Integer>(comp);
    keys.addAll(treeMap.keySet());
    for(Integer key: keys){
      if(key==1){
        System.out.println("------Root node-----");
      }else if(key == keys.last()){
        System.out.println("------Leaf nodes-----");
      }else{
        System.out.println("------Inner nodes-----");
      }
      for(Node node: treeMap.get(key)){
        if (node.numKeys == 0) {
          System.out.println("First key: "+null+" Last Key: "+null);
        } else if (node.numKeys == 1){
          System.out.println("First key: "+node.keys[0]+" Last Key: "+null);
        }else {
          System.out.println("First key: "+node.keys[0]+" Last Key: "+node.keys[node.numKeys-1]);
        }
      }
    }
  }

  /**
   * depth of the tree obtained by traversing the tree.
   * 
   * @return
   */

  private int getDepth() {
    if (root.numKeys == 0) {
      return 0;
    }
    int count = 1;
    Node node = root;
    while (!node.isLeaf) {
      for (int i = 0; i < node.numKeys + 1; i++) {
        if (node.children[i] != null) {
          // System.out.println(node.numKeys + " " + node.isLeaf + " " + node.keys[i]);
          node = node.children[i];
          count++;
          break;
        }
      }
    }
    return count;
  }

  /**
   * number of blocks
   * 
   * @param node
   * @return
   */

  private int getNumberOfBlocks(Node node) {
    if (root.numKeys == 0) {
      return 0;
    } else if (root.isLeaf) {
      return 1;
    }
    int i = 1;
    while (node.nextNode != null) {
      i++;
      node = node.nextNode;
    }
    return i;
  }

  /**
   * list all the keys
   * 
   * @nextNode is the 11th pointer described in the assignment.
   */

  @Override
  public void list() {
    Node leafNode = getFirstLeafNode(root);
    list(leafNode);
  }

  private void list(Node node) {
    for (int i = 0; i < node.keys.length; i++) {
      if (node.keys[i] != null && !"".equalsIgnoreCase(node.keys[i])) {
        System.out.println(node.keys[i]);
      }
    }
    if (node.nextNode != null) {
      list(node.nextNode);
    }
  }

  /**
   * returns the first leaf node.
   * 
   * @param node
   * @return
   */

  private Node getFirstLeafNode(Node node) {
    if (node.isLeaf) {
      return node;
    }
    while (!node.isLeaf) {
      for (int i = 0; i < node.numKeys + 1; i++) {
        if (node.children[i] != null) {
          node = node.children[i];
          break;
        }
      }
    }
    return node;
  }

  /**
   * not used
   * get first key
   * 
   * @return
   */

  private String getFirstKey() {
    Node node = root;
    while (true) {
      if (node.isLeaf) {
        break;
      } else if (node.children[0].isLeaf) {
        break;
      } else {
        node = node.children[0];
      }
    }
    return node.keys[0];
  }

  /**
   * not used
   * get last key
   * 
   * @return
   */
  private String getLastKey() {
    Node node = root;
    while (true) {
      if (node.isLeaf) {
        break;
      } else if (node.children[node.numKeys].isLeaf) {
        break;
      } else {
        node = node.children[node.numKeys];
      }
    }
    if (node.numKeys >= 1) {
      return node.keys[node.numKeys - 1];
    } else {
      return null;
    }

  }

  /**
   * test bplus tree
   * 
   * @param args
   */
  public static void main(String[] args) {
    BPlusTree tree = new BPlusTree();

    tree.put("1","      ");
    tree.put("2","      ");
    tree.put("3","      ");
    tree.put("4","      ");
    tree.put("5","      ");
    tree.put("6","      ");
    tree.put("7","      ");
    tree.put("8","      ");
    tree.put("9","      ");
    tree.put("10","      ");
    tree.put("11","      ");
    tree.put("12","      ");
    tree.put("13","      ");
    tree.put("14","      ");
    tree.put("15","      ");
    
    System.out.println(tree.get("8"));
    System.out.println(tree.root.children[1].keys[5]);
    
    tree.put("16","      ");
    tree.put("17","      ");
    tree.put("18","      ");
    tree.put("19","      ");
    tree.put("20","      ");
    
    tree.put("21","      ");
    tree.put("22","      ");
    tree.put("23","      ");
    tree.put("24","      ");
    tree.put("25","      ");
    tree.put("26","      ");
    tree.put("27","      ");
    tree.put("28","      ");
    tree.put("29","      ");
    tree.put("30","      ");
    tree.put("31","      ");
    tree.put("32","      ");
    tree.put("33","      ");
    tree.put("34","      ");
    tree.put("35","      ");
    tree.put("36","      ");
    tree.put("37","      ");
    tree.put("38","      ");
    tree.put("39","      ");
    tree.put("40","      ");
    
    tree.put("41","      ");
    tree.put("42","      ");
    tree.put("43","      ");
    tree.put("44","      ");
    tree.put("45","      ");
    tree.put("46","      ");
    tree.put("47","      ");
    tree.put("48","      ");
    tree.put("49","      ");
    tree.put("50","      ");
    tree.put("51","      ");
    tree.put("52","      ");
    tree.put("53","      ");
    tree.put("54","      ");
    tree.put("55","      ");
    tree.put("56","      ");
    tree.put("57","      ");
    tree.put("58","      ");
    System.out.println("_____________________");
    System.out.println(tree.root.keys[0]);
    System.out.println(tree.root.keys[1]);
    System.out.println(tree.root.children[0].keys[0]);
    System.out.println(tree.root.children[0].keys[1]);
    System.out.println(tree.root.children[0].keys[2]);
    System.out.println(tree.root.children[0].keys[3]);
    System.out.println(tree.root.children[1].keys[0]);
    System.out.println(tree.root.children[1].keys[1]);
    System.out.println(tree.root.children[1].keys[2]);
    System.out.println(tree.root.children[1].keys[3]);
    System.out.println(tree.root.children[1].keys[4]);
    System.out.println(tree.root.children[1].keys[5]);
    System.out.println(tree.root.children[1].keys[6]);
    System.out.println(tree.root.children[1].children[8]);
    
    /*
    tree.put("59","      ");
    tree.put("60","      ");
    tree.put("61","      ");
    tree.put("62","      ");
    tree.put("63","      ");
    tree.put("64","      ");
    tree.put("65","      ");
    tree.put("66","      ");
    tree.put("67","      ");
    tree.put("68","      ");
    tree.put("69","      ");
    tree.put("70","      ");
    tree.put("71","      ");
    tree.put("72","      ");
    tree.put("73","      ");
    tree.put("74","      ");
    tree.put("75","      ");
    tree.put("76","      ");
    tree.put("77","      ");
    tree.put("78","      ");
    tree.put("79","      ");
    tree.put("80","      ");
    tree.put("81","      ");
    tree.put("82","      ");
    tree.put("83","      ");
    tree.put("84","      ");
    tree.put("85","      ");
    tree.put("86","      ");
    tree.put("87","      ");
    tree.put("88","      ");
    tree.put("89","      ");
    tree.put("90","      ");
    tree.put("91","      ");
    tree.put("92","      ");
    tree.put("93","      ");
    tree.put("94","      ");
    tree.put("95","      ");
    tree.put("96","      ");
    tree.put("97","      ");
    tree.put("98","      ");
    tree.put("99","      ");
    tree.put("100","      ");
    tree.put("101","      ");
    tree.put("102","      ");
    tree.put("103","      ");
    tree.put("104","      ");
    tree.put("105","      ");
    tree.put("106","      ");
    tree.put("107","      ");
    tree.put("108","      ");
    tree.put("109","      ");
    tree.put("110","      ");
    tree.put("111","      ");
    tree.put("112","      ");
    tree.put("113","      ");
    tree.put("114","      ");
    tree.put("115","      ");
    tree.put("116","      ");
    tree.put("117","      ");
    tree.put("118","      ");
    tree.put("119","      ");
    tree.put("120","      ");
    tree.put("121","      ");
    tree.put("122","      ");
    tree.put("123","      ");
    tree.put("124","      ");
    tree.put("125","      ");
    tree.put("126","      ");
    tree.put("127","      ");
    tree.put("128","      ");
    tree.put("129","      ");
    tree.put("130","      ");
    tree.put("131","      ");
    tree.put("132","      ");
    tree.put("133","      ");
    tree.put("134","      ");
    tree.put("135","      ");
    tree.put("136","      ");
    tree.put("137","      ");
    tree.put("138","      ");
    tree.put("139","      ");
    tree.put("140","      ");
    tree.put("141","      ");
    tree.put("142","      ");
    tree.put("143","      ");
    tree.put("144","      ");
    tree.put("145","      ");
    tree.put("146","      ");
    tree.put("147","      ");
    tree.put("148","      ");
    tree.put("149","      ");
    tree.put("150","      ");
    tree.put("151","      ");
    tree.put("152","      ");
    tree.put("153","      ");
    tree.put("154","      ");
    tree.put("155","      ");
    tree.put("156","      ");
    tree.put("157","      ");
    tree.put("158","      ");
    tree.put("159","      ");
    tree.put("160","      ");
    tree.put("161","      ");
    tree.put("162","      ");
    tree.put("163","      ");
    tree.put("164","      ");
    tree.put("165","      ");
    tree.put("166","      ");
    tree.put("167","      ");
    tree.put("168","      ");
    tree.put("169","      ");
    tree.put("170","      ");
    tree.put("171","      ");
    tree.put("172","      ");
    tree.put("173","      ");
    tree.put("174","      ");
    tree.put("175","      ");
    tree.put("176","      ");
    tree.put("177","      ");
    tree.put("178","      ");
    tree.put("179","      ");
    tree.put("180","      ");
    tree.put("181","      ");
    tree.put("182","      ");
    tree.put("183","      ");
    tree.put("184","      ");
    tree.put("185","      ");
    tree.put("186","      ");
    tree.put("187","      ");
    tree.put("188","      ");
    tree.put("189","      ");
    tree.put("190","      ");
    tree.put("191","      ");
    tree.put("192","      ");
    tree.put("193","      ");
    tree.put("194","      ");
    tree.put("195","      ");
    tree.put("196","      ");
    tree.put("197","      ");
    tree.put("198","      ");
    tree.put("199","      ");
    tree.put("200","      ");
    tree.put("201","      ");
    tree.put("202","      ");
    tree.put("203","      ");
    tree.put("204","      ");
    tree.put("205","      ");
    tree.put("206","      ");
    tree.put("207","      ");
    tree.put("208","      ");
    tree.put("209","      ");
    tree.put("210","      ");
    tree.put("211","      ");
    tree.put("212","      ");
    tree.put("213","      ");
    tree.put("214","      ");
    tree.put("215","      ");
    tree.put("216","      ");
    tree.put("217","      ");
    tree.put("218","      ");
    tree.put("219","      ");
    tree.put("220","      ");
    tree.put("221","      ");
    tree.put("222","      ");
    tree.put("223","      ");
    tree.put("224","      ");
    tree.put("225","      ");
    tree.put("226","      ");
    tree.put("227","      ");
    tree.put("228","      ");
    tree.put("229","      ");
    tree.put("230","      ");
    tree.put("231","      ");
    tree.put("232","      ");
    tree.put("233","      ");
    tree.put("234","      ");
    tree.put("235","      ");
    tree.put("236","      ");
    tree.put("237","      ");
    tree.put("238","      ");
    tree.put("239","      ");
    */
    /*
    tree.update("1", "confidential information");
    System.out.println("-------");
    System.out.println(tree.get("1").getValue());
    System.out.println("-------");

    tree.snapshot();

    // System.out.println(tree.root.children[1].children[7].keys[5]);

    // System.out.println("get first leaf node: " + tree.getFirstLeafNode(tree.root).keys[0]);
    System.out.println("-------");
    System.out.println(tree.get("9").getValue());
    System.out.println("-------");
    System.out.println(tree.root.children[2].keys[4]);
    // System.out.println(tree.root.children[1].children[7].children[6].keys[6]);
    // System.out.println(tree.root.children[1].children[7].children[6].numKeys);

    tree.delete("9");
    tree.delete("8");
    tree.delete("7");
    tree.delete("6");
    tree.delete("5");

    System.out.println("----" + tree.root.children[1].numKeys);

    tree.delete("4");
    tree.delete("3");
    tree.delete("2");
    tree.delete("15");
    tree.delete("14");
    tree.delete("13");
    
    System.out.println("----" + tree.root.keys[0]);

    tree.delete("12");
    tree.delete("11");
    tree.delete("10");
    tree.delete("1");
    */
    
    tree.snapshot();
    System.out.println("-------");

    tree.delete("1");
    System.out.println("-------");
    tree.delete("2");
    System.out.println("-------");
    tree.delete("3");
    System.out.println("-------");
    tree.delete("4");
    System.out.println("-------");
    tree.delete("5");
    System.out.println("-------");
    tree.delete("6");
    System.out.println("-------");
    tree.delete("7");
    System.out.println("-------");
    tree.delete("8");
    System.out.println("-------");
    tree.delete("9");
    System.out.println("-------");
    tree.delete("10");
    System.out.println("-------");
    tree.delete("11");
    System.out.println("-------");
    tree.delete("12");
    System.out.println("-------");
    tree.delete("13");
    System.out.println("-------");
    tree.delete("14");
    System.out.println("-------");
    tree.delete("15");
    tree.delete("16");
    tree.delete("17");
    tree.delete("18");
    tree.delete("19");
    tree.delete("20");
    tree.delete("21");
    tree.delete("22");
    tree.delete("23");
    tree.snapshot();
    tree.delete("24");
    tree.delete("25");
    tree.delete("26");
    tree.delete("27");
    tree.delete("28");
    tree.delete("29");
    tree.delete("30");
    tree.delete("31");
    tree.delete("32");
    tree.delete("33");
    tree.delete("34");
    tree.delete("35");
    tree.delete("36");
    tree.delete("37");
    tree.delete("38");
    tree.delete("39");
    tree.delete("40");
    tree.delete("41");
    tree.delete("42");
    tree.delete("43");
    tree.delete("44");
    tree.snapshot();
    tree.delete("45");
    tree.delete("46");
    tree.delete("47");
    tree.delete("48");
    tree.delete("49");
    tree.delete("50");
    tree.delete("51");
    tree.delete("52");
    tree.delete("53");
    tree.delete("54");
    tree.delete("55");
    tree.delete("56");
    tree.list();
    tree.snapshot();
    tree.delete("57");
    tree.delete("58");
    /*
    tree.delete("59");
    tree.delete("60");
    tree.delete("61");
    tree.delete("62");
    tree.delete("63");
    tree.delete("64");
    tree.delete("65");
    tree.delete("66");
    tree.delete("67");
    tree.delete("68");
    tree.delete("69");
    tree.delete("70");
    tree.delete("71");
    tree.delete("72");
    tree.delete("73");
    tree.delete("74");
    tree.delete("75");
    tree.delete("76");
    tree.delete("77");
    tree.delete("78");
    tree.delete("79");
    tree.delete("80");
    tree.delete("81");
    tree.delete("82");
    tree.delete("83");
    tree.delete("84");
    tree.delete("85");
    tree.delete("86");
    tree.delete("87");
    tree.delete("88");
    tree.delete("89");
    tree.delete("90");
    tree.delete("91");
    tree.delete("92");
    tree.delete("93");
    tree.delete("94");
    tree.delete("95");
    tree.delete("96");
    tree.delete("97");
    tree.delete("98");
    tree.delete("99");
    tree.delete("100");
    tree.delete("101");
    tree.delete("102");
    tree.delete("103");
    tree.delete("104");
    tree.delete("105");
    tree.delete("106");
    tree.delete("107");
    tree.delete("108");
    tree.delete("109");
    tree.delete("110");
    tree.delete("111");
    tree.delete("112");
    tree.delete("113");
    tree.delete("114");
    tree.delete("115");
    tree.delete("116");
    tree.delete("117");
    tree.delete("118");
    tree.delete("119");
    tree.delete("120");
    tree.delete("121");
    tree.delete("122");
    tree.delete("123");
    tree.delete("124");
    tree.delete("125");
    tree.delete("126");
    tree.delete("127");
    tree.delete("128");
    tree.delete("129");
    tree.delete("130");
    tree.delete("131");
    tree.delete("132");
    tree.delete("133");
    tree.delete("134");
    tree.delete("135");
    tree.delete("136");
    tree.delete("137");
    tree.delete("138");
    tree.delete("139");
    tree.delete("140");
    tree.delete("141");
    tree.delete("142");
    tree.delete("143");
    tree.delete("144");
    tree.delete("145");
    tree.delete("146");
    tree.delete("147");
    tree.delete("148");
    tree.delete("149");
    tree.delete("150");
    tree.delete("151");
    tree.delete("152");
    tree.delete("153");
    tree.delete("154");
    tree.delete("155");
    tree.delete("156");
    tree.delete("157");
    tree.delete("158");
    tree.delete("159");
    tree.delete("160");
    tree.delete("161");
    tree.delete("162");
    tree.delete("163");
    tree.delete("164");
    tree.delete("165");
    tree.delete("166");
    tree.delete("167");
    tree.delete("168");
    tree.delete("169");
    tree.delete("170");
    tree.delete("171");
    tree.delete("172");
    tree.delete("173");
    tree.delete("174");
    tree.delete("175");
    tree.delete("176");
    tree.delete("177");
    tree.delete("178");
    tree.delete("179");
    tree.delete("180");
    tree.delete("181");
    tree.delete("182");
    tree.delete("183");
    tree.delete("184");
    tree.delete("185");
    tree.delete("186");
    tree.delete("187");
    tree.delete("188");
    tree.delete("189");
    tree.delete("190");
    tree.delete("191");
    tree.delete("192");
    tree.delete("193");
    tree.delete("194");
    tree.delete("195");
    tree.delete("196");
    tree.delete("197");
    tree.delete("198");
    tree.delete("199");
    tree.delete("200");
    tree.delete("201");
    tree.delete("202");
    tree.delete("203");
    
    tree.delete("204");
    tree.delete("205");
    tree.delete("206");
    tree.delete("207");
    System.out.println("----------");
    System.out.println(tree.root.isLeaf);
    tree.delete("208");
    System.out.println("----------");
    System.out.println(tree.root.isLeaf);
    //tree.delete("209");
    */
    /*
    tree.delete("210");
    tree.delete("211");
    tree.delete("212");
    tree.delete("213");
    tree.delete("214");
    tree.delete("215");
    tree.delete("216");
    tree.delete("217");
    tree.delete("218");
    tree.delete("219");
    tree.delete("220");
    tree.delete("221");
    tree.delete("222");
    tree.delete("223");
    tree.delete("224");
    tree.delete("225");
    tree.delete("226");
    tree.delete("227");
    tree.delete("228");
    tree.delete("229");
    tree.delete("230");
    tree.delete("231");
    tree.delete("232");
    tree.delete("233");
    tree.delete("234");
    tree.delete("235");
    tree.delete("236");
    tree.delete("237");
    tree.delete("238");
    tree.delete("239");
    */
    tree.snapshot();
    
    System.out.println(tree.root.keys[0]);

  }

}
