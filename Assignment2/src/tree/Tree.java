package tree;


/**
 * Tree interface
 * 
 * @author ravikiran
 *
 */
public interface Tree {

  public void put(String key, String value);

  public void update(String key, String value);

  public void snapshot();

  public void list();

  public void delete(String key);

  public KeyValue get(String key);

  public class KeyValue {
    private String key;
    private String value;

    KeyValue(String key, String value) {
      this.key = key;
      this.value = value;
    }

    public String getKey() {
      return key;
    }

    public void setKey(String key) {
      this.key = key;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }
  }

}
