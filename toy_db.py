# database assignment 1 - team assignment

class DataDictionary():  # In Progress
    """ The dictionary is supposed to have one entry per attribute """
    def __init__(self, tab_name, att_name, att_type, tab_file_name):
        # This is assuming all the information is sent to the function on creation. Is it?
        table_name = self.tab_name  # name of table containing attribute
        attribute_name = self.att_name  # name of attribute
        attribute_type = self.att_type  # attribute type, I or S for INT or STRING
        table_file_name = self.tab_file_name  # name of the file in which table is stored


def handle_error(error_string):
    """ Currently prints the error string; could be changed to save to an error log file. """
    print "Error:", error_string


def parse_by_given_method(cmd, parse_by):
    return cmd.split(parse_by)


def create_table(cmd): # In Progress -- need to separate names and types
    """ Parses the CREATE command and creates a table """
    info_list = parse_by_given_method(cmd, "(")
    cmd_string = info_list[1][:-1]
    info_list = parse_by_given_method(cmd_string, ",")

    # need to actually create the table now
    print "Need to create table with the following attribute names and types:"
    print info_list
    print


def insert_into_table(cmd): # In Progress -- not parsing quite right (extra spaces, etc.)
    info_list = parse_by_given_method(cmd, "(")
    cmd_string = info_list[1][:-1]
    info_list = parse_by_given_method(cmd_string, ",")

    # need to actually insert into the table now
    print "Need to insert the following names and values into the table:"
    print info_list
    print


def select_info(cmd): # In Progress

    if cmd[:7] == "* FROM ":
        info = cmd[7:].split(" WHERE ")
        table_list = info[0].split(",")
        where_string = info[1]
        print "From", table_list, " need to know ALL of where the following occurs:"
        print where_string
        print
    else:
        info = cmd.split(" FROM ")
        field_names_list = info[0].split(",")
        table_name = info[1]
        print "From", table_name, "need the following field names:"
        print field_names_list
        print

    # need to actually select (and display?) now


def run_cmd(cmd):
    """ Parse and run command """
    if len(cmd) > 13:  # minimum length possible for a valid command
        if cmd[:7] == "CREATE ":
            create_table(cmd[7:])
        elif cmd[:12] == "INSERT INTO ":
            insert_into_table(cmd[12:])
        elif cmd[:7] == "SELECT ":
            select_info(cmd[7:])
        else:
            handle_error("invalid command")
            return -1
    else:
        handle_error("invalid command (too short)")
        return -1


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def main(): # In Progress

    print
    print "Begin program ---"
    print

    # Could get the following commands through raw_input() but this is easier for testing
    # Note: we need to make sure apostrophes in book names don't mess things up (left it out of the query example)
    sample_query_1 = "CREATE books( callno INT, title STRING, authors STRING, year INT)"
    sample_query_2 = "INSERT INTO books (1, 'The Devils Dictionary', 'Ambrose Bierce', 1998)"
    sample_query_3 = "SELECT * FROM books WHERE year = 1998"
    sample_query_4 = "SELECT callno, title FROM books"
    run_cmd(sample_query_1)
    run_cmd(sample_query_2)
    run_cmd(sample_query_3)
    run_cmd(sample_query_4)

    print
    print "--- End program"


if __name__ == "__main__":
   main()