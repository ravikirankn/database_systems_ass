package tree;

public class BTree implements Tree {

  private static final int maxChildren = 5;

  private static final class Node {
    private int numChildren;
    private Child[] child = new Child[maxChildren];

    private Node(int k) {
      numChildren = k;
    }
  }

  public static class Child {
    private String key;
    private String value;
    private Node next;

    public Child(String key, String value, Node next) {
      this.key = key;
      this.value = value;
      this.next = next;
    }

    public String getKey() {
      return key;
    }

    public void setKey(String key) {
      this.key = key;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }

  }

  private Node root;
  private int height;
  private int numKeys;

  public BTree() {
    root = new Node(0);
  }

  public int size() {
    return numKeys;
  }

  public int height() {
    return height;
  }

  /**
   * Insert a key into bplus tree
   * 
   * @param key
   * @param value
   */
  public void put(String key, String value) {
    Node u = insert(root, key, value, height);
    numKeys++;
    if (u == null) return;

    // split root
    Node t = new Node(2);
    t.child[0] = new Child(root.child[0].key, null, root);
    t.child[1] = new Child(u.child[0].key, null, u);
    root = t;
    height++;
  }

  /**
   * Delete a key from bplus tree
   * 
   * @param key
   */
  public void delete(String key) {
    Child x = search(root, key, height);
    String delKey = x.key;
    System.out.println("Delete implementation pending.");
    // need to make this function actually delete the key
    //

  }

  private Node insert(Node node, String key, String value, int height) {
    int j;
    Child t = new Child(key, value, null);

    // external node
    if (height == 0) {
      for (j = 0; j < node.numChildren; j++) {
        if (less(key, node.child[j].key)) break;
      }
    }

    // internal node
    else {
      for (j = 0; j < node.numChildren; j++) {
        if ((j + 1 == node.numChildren) || less(key, node.child[j + 1].key)) {
          Node u = insert(node.child[j++].next, key, value, height - 1);
          if (u == null) return null;
          t.key = u.child[0].key;
          t.next = u;
          break;
        }
      }
    }

    for (int i = node.numChildren; i > j; i--)
      node.child[i] = node.child[i - 1];
    node.child[j] = t;
    node.numChildren++;
    if (node.numChildren < maxChildren)
      return null;
    else
      return split(node);
  }

  /**
   * Search for a key
   * 
   * @param key
   * @return Child
   */

  public KeyValue get(String key) {
    Child child = search(root, key, height);
    return (new KeyValue(child.getKey(), child.getValue()));
  }

  private Child search(Node node, String key, int height) {
    Child[] children = node.child;
    if (height == 0) {
      for (int j = 0; j < node.numChildren; j++) {
        if (key.compareTo(children[j].key) == 0) return children[j];
      }
    } else {
      for (int j = 0; j < node.numChildren; j++) {
        if (j + 1 == node.numChildren || less(key, children[j + 1].key))
          return search(children[j].next, key, height - 1);
      }
    }
    return null;
  }

  /**
   * update a key
   * 
   * @param key
   * @param value
   */

  public void update(String key, String value) {
    Child child = search(root, key, height);
    if (child != null) {
      child.setValue(value);
    } else {
      System.out.println("Key doesn't exist.");
    }
  }

  /**
   * snapshot of the tree
   */

  public void snapshot() {
    System.out.println("Number of records: " + size());
    System.out.println("Depth of the tree: " + height());
    System.out.println("First key: ");
    System.out.println("Last key: ");
    System.out.println("Number of blocks occupied: ");
  }

  /**
   * list all the values in the tree in order
   */

  public void list() {
    list(root, height);
  }

  private void list(Node node, int height) {
    Child[] children = node.child;
    if (height == 0) {
      for (int j = 0; j < node.numChildren; j++) {
        System.out.println(children[j].key);
      }
    } else {
      for (int j = 0; j < node.numChildren; j++) {
        list(children[j].next, height - 1);
      }
    }
  }

  /**
   * compare keys
   * 
   * @param k1
   * @param k2
   * @return
   */

  private boolean less(String k1, String k2) {
    return k1.compareTo(k2) < 0;
  }

  /**
   * split node
   * 
   * @param h
   * @return
   */
  private Node split(Node h) {
    Node t = new Node(maxChildren / 2);
    h.numChildren = maxChildren / 2;
    for (int j = 0; j < maxChildren / 2; j++)
      t.child[j] = h.child[maxChildren / 2 + j];
    return t;
  }

  /**
   * test bplus tree
   * 
   * @param args
   */
  public static void main(String[] args) {
    BTree tree = new BTree();

    tree.put("aa,bb", "     ");
    tree.put("ab,aa", "     ");
    tree.put("xy,xx", "     ");
    tree.put("xy,aa", "     ");
    tree.put("aa,bb", "     ");
    tree.put("cc,dd", "     ");

    System.out.println("aa,bb:  " + tree.get("aa,bb").getKey());

    tree.update("aa,bb", "confidential information 1");

    System.out.println("aa,bb:  " + tree.get("aa,bb").getValue());

    System.out.println("-------");

    System.out.println("size:    " + tree.size());
    System.out.println("height:  " + tree.height());
    System.out.println();

    tree.list();
    tree.snapshot();
  }

}
